This app removes chunks metadata from objects in s3-compatible object storage.

# Problem
By default `enable_signature_v4_streaming` is `true` in GitLab. That means that files are splitted by chunks and every chunk is [signed](https://docs.aws.amazon.com/AmazonS3/latest/API/sigv4-streaming.html) before uploading:
```
10000;chunk-signature=0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef\r\nCHUNK_DATA\r\n
...
0;chunk-signature=0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef\r\n\r\n
```
As I understand, Fog (a library that is used in GitLab), doesn't set `transfer-encoding=chunked` or `content-encoding=aws-chunked` header but only `x-amz-content-sha256=STREAMING-AWS4-HMAC-SHA256-PAYLOAD` ([link](https://github.com/fog/fog-aws/blob/v3.5.2/lib/fog/aws/storage.rb#L591)). Some object storage providers doesn't set chunked mode based on `x-amz-content-sha256` and interprets chunks metadata as part of objects. This breaks `git lfs checkout` because OIDs computed for downloaded files differs from OIDs in lfs pointers. To fix this you need to set `enable_signature_v4_streaming=false` and remove all chunks metadata in all files in object storage.

# Configuration
You can see all configurations options in `docker-compose.yaml`. Bucket names must be separated by comma.
