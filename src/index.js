const http = require("http")
const Minio = require("minio")

const semiCharCode = ";".charCodeAt(0)
const endHeaderLen = 18 + 64 + 2 + 2
const logLines = []

const log = (...line) => {
  const timestamp = new Date().toISOString()
  console.log(timestamp, ...line)
  logLines.push([timestamp].concat(line).join(" "))
}

/**
 * @param {number} ms
 * @returns {Promise<void>}
 */
const sleep = ms =>
  new Promise(resolve => {
    setTimeout(resolve, ms)
  })

/**
 * @template T
 * @param {number} ms
 * @param {() => Promise<T>)} executor
 * @returns {Promise<T>}
 */
const retry = (ms, executor) =>
  executor().catch(async err => {
    log("retry error", err.message)
    await sleep(ms)
    return retry(ms, executor)
  })

/**
 * @param {Buffer} buf
 * @param {number} pos
 * @param {number} len
 * @returns {Buffer}
 */
const bufFrom = (buf, pos, len) =>
  Buffer.from(buf.buffer, buf.byteOffset + pos, len)

/**
 * @param {Buffer} buf
 * @param {number} pos
 * @param {number} end
 * @param {number} l
 * @returns {[number, string] | null}
 */
const readString = (buf, pos, end, l) => {
  for (let i = 0; i < l; i++) {
    if (buf[pos + i] === end) {
      return [i, bufFrom(buf, pos, i).toString()]
    }
  }
  return null
}

/**
 * @param {Buffer} buf
 * @param {number} pos
 * @returns {[number, Buffer] | null}
 */
const readChunk = (buf, pos) => {
  const maybeSizeHex = readString(buf, pos, semiCharCode, 6)
  if (maybeSizeHex === null) {
    log("error SizeNotFound", pos, bufFrom(buf, pos, 6).toString())
    return null
  }
  const [sizeHexLen, sizeHex] = maybeSizeHex
  const header = bufFrom(buf, pos + sizeHexLen, 17).toString()
  if (header !== ";chunk-signature=") {
    log("error InvalidHeader", header)
    return null
  }
  const headerLen = sizeHexLen + 17 + 64 + 2
  log(bufFrom(buf, pos, headerLen).toString())

  const size = parseInt(sizeHex, 16)
  if (isNaN(size) || 65536 < size || size < 0) {
    log("error InvalidSize", size)
    return null
  }
  return [headerLen + size + 2, bufFrom(buf, pos + headerLen, size)]
}

/**
 * @param {Buffer} buf
 * @returns {Buffer | null}
 */
const fixBuf = buf => {
  const chunks = []
  let pos = 0
  while (pos < buf.byteLength) {
    const result = readChunk(buf, pos)
    if (result === null) return null
    const [consumed, chunk] = result
    if (chunk.byteLength === 0) return Buffer.concat(chunks)
    chunks.push(chunk)
    pos += consumed
  }
}

/**
 * @template T
 * @param {Minio.BucketStream<T>} stream
 * @returns {Promise<Array<T>>}
 */
const readStream = stream =>
  new Promise((resolve, reject) => {
    const chunks = []
    stream.on("error", reject)
    stream.on("data", chunk => {
      chunks.push(chunk)
    })
    stream.on("end", () => {
      resolve(chunks)
    })
  })

/**
 * @param {Minio.Client} minioClient
 * @param {string} bucketName
 * @param {Minio.BucketItem} obj
 * @returns {Promise<string | null>}
 */
const fixObj = async (minioClient, bucketName, obj) => {
  if (obj.size < endHeaderLen) return Promise.resolve(null)
  const endOffset = obj.size - endHeaderLen
  const end = await retry(2000, () =>
    minioClient
      .getPartialObject(bucketName, obj.name, endOffset, 18)
      .then(readStream)
  )
  if (Buffer.concat(end).toString() !== "0;chunk-signature=")
    return Promise.resolve(null)

  log("object", bucketName, obj.name)
  const data = await retry(2000, () =>
    minioClient.getObject(bucketName, obj.name).then(readStream)
  )
  const fixed = fixBuf(Buffer.concat(data))
  if (fixed === null) return Promise.resolve(null)
  const etag = await retry(2000, () =>
    minioClient.putObject(bucketName, obj.name, fixed)
  )
  log("etag", etag, fixed.byteLength)
  return etag
}

const main = async () => {
  const minioClient = new Minio.Client({
    endPoint: process.env.MINIO_ENDPOINT,
    region: process.env.MINIO_REGION,
    accessKey: process.env.MINIO_ACCESSKEY,
    secretKey: process.env.MINIO_SECRETKEY
  })
  const bucketNames = process.env.MINIO_BUCKETNAMES.split(",")
  for (const bucketName of bucketNames) {
    const list = await readStream(minioClient.listObjects(bucketName, "", true))
    for (const obj of list) {
      await fixObj(minioClient, bucketName, obj)
    }
    log("done", bucketName, list.length)
  }
}

const srv = http.createServer((req, res) => {
  const userAgent = req.headers["user-agent"]
  if (typeof userAgent === "string" && userAgent.startsWith("kube-probe/")) {
    res.writeHead(200)
    res.end()
    return
  }
  if (req.url === "/log") {
    const data = JSON.stringify(logLines)
    res.writeHead(200, {
      "content-type": "application/json",
      "content-length": Buffer.byteLength(data)
    })
    res.end(data)
    return
  }
  res.writeHead(404)
  res.end()
})
srv.listen(5000, () => {
  main().catch(err => {
    log("main error", err.message)
  })
})
